const express = require("express");

const { s3uploadGallery } = require("../middleware/s3upload");

const router = express.Router();
const {
  createDestination,
  getAllDestination,
  getDestinationById,
  getDestinationByLocation,
  getDestinationByName,
  getDestinationByBudget,
  getDestinationByDate,
  updateDestination,
  deleteDestination,
} = require("../controllers/destination-cont");

const { roleAdmin, roleHost, roleAdminAndHost } = require("../middleware/auth");

router.get("/destination/all", getAllDestination);

router.get("/destination", getDestinationById); //FILTER BY DESTINATION ID HERE

router.get("/destination/name/search", getDestinationByName); //FILTER BY DESTINATION NAME HERE

router.get("/destination/budget/search", getDestinationByBudget); //FILTER BY DESTINATION BUDGET HERE

router.get("/destination/date/search", getDestinationByDate); //FILTER BY DESTINATION DATE/TIME HERE

router.post(
  "/destination/create",
  roleHost,
  s3uploadGallery,
  createDestination
);

router.put("/destination/upd", roleHost, s3uploadGallery, updateDestination);

router.delete("/destination/del", roleAdminAndHost, deleteDestination);

module.exports = router;